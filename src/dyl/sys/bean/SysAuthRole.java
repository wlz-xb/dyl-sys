/**
 * 由 EasyCode自动生成
 * @author Dyl
 * 2017-04-19 17:09:00
*/
package dyl.sys.bean;

import java.io.Serializable;
import com.alibaba.fastjson.annotation.JSONField;
import java.math.BigDecimal;
import java.util.Date;

public class SysAuthRole implements Serializable {
    private static final long serialVersionUID = 1L;
	/*字段说明：
	*对应db字段名:id 类型:NUMBER(22) 主键 
	*是否可以为空:是
	*/
	
	private  BigDecimal  id;
	
	/*字段说明：角色id
	*对应db字段名:role_id 类型:NUMBER(22)  
	*是否可以为空:是
	*/
	
	private  BigDecimal  roleId;
	
	/*字段说明：菜单id
	*对应db字段名:menu_id 类型:NUMBER(22)  
	*是否可以为空:是
	*/
	
	private  BigDecimal  menuId;
	
	/*字段说明：权限id
	*对应db字段名:kind_id 类型:NUMBER(22)  
	*是否可以为空:是
	*/
	
	private  BigDecimal  kindId;
	
	/*字段说明：创建时间
	*对应db字段名:create_time 类型:DATE(7)  
	*是否可以为空:是
	*/
	@JSONField(format = "yyyy-MM-dd HH:mm:ss")
	private  Date  createTime;
	
    public BigDecimal getId() {
        return id;
    }
    public void setId(BigDecimal id) {
        this.id = id;
    }
    public BigDecimal getRoleId() {
        return roleId;
    }
    public void setRoleId(BigDecimal roleId) {
        this.roleId = roleId;
    }
    public BigDecimal getMenuId() {
        return menuId;
    }
    public void setMenuId(BigDecimal menuId) {
        this.menuId = menuId;
    }
    public BigDecimal getKindId() {
        return kindId;
    }
    public void setKindId(BigDecimal kindId) {
        this.kindId = kindId;
    }
    public Date getCreateTime() {
        return createTime;
    }
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }
	public String toString() {
		return 
		"id:"+id+"\n"+
		
		"roleId:"+roleId+"\n"+
		
		"menuId:"+menuId+"\n"+
		
		"kindId:"+kindId+"\n"+
		
		"createTime:"+createTime+"\n"+
		"";
	}
}
