package dyl.sys.bean;

import java.io.Serializable;

/**
 * Ztree Bean
 * 
 * @author Dyl
 * 
 */
public class Ztree implements Serializable {
	private static final long serialVersionUID = 1L;
	private String id;// 主键
	private String pId;// 父Id
	private String name;// 节点名称
	private boolean open = true;// 是否打开
	private boolean checked;//是否选中
	private Integer index;//顺序
	private	boolean nocheck = false;
	
	
	public boolean isNocheck() {
		return nocheck;
	}
	public void setNocheck(boolean nocheck) {
		this.nocheck = nocheck;
	}
	public Integer getIndex() {
		return index;
	}
	public void setIndex(Integer index) {
		this.index = index;
	}
	public void setChecked(boolean checked) {
		this.checked = checked;
	}
	public boolean isChecked() {
		return checked;
	}
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getpId() {
		return pId;
	}

	public void setpId(String pId) {
		this.pId = pId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public boolean isOpen() {
		return open;
	}

	public void setOpen(boolean open) {
		this.open = open;
	}

}
