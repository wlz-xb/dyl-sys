package dyl.sys.service;


import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import dyl.common.util.JdbcTemplateUtil;
import dyl.common.util.Page;
import dyl.sys.bean.SysFileinfo;
/**

 * @author Dyl
 * 2017-06-06 15:31:42
 */
@Service
@Transactional
public class SysFileinfoServiceImpl {
	@Resource
	private JdbcTemplateUtil jdbcTemplate;
	/**
	 * 说明：分页查询表SYS_FILEINFO记录封装成List集合
	 * @return List<SysFileinfo
	 */
	public List<SysFileinfo>  findSysFileinfoList(Page page,SysFileinfo sysFileinfo) throws Exception{
		String sql = "select id,real_name,upload_name,file_size,file_url,creator,create_time,c_id from SYS_FILEINFO t where 1=1";
		List<Object> con = new ArrayList<Object>();
		if(sysFileinfo.getId()!=null){
			sql+=" and t.id=?";
			con.add(sysFileinfo.getId());
		}
		if(StringUtils.isNotEmpty(sysFileinfo.getRealName())){
			sql+=" and t.real_name like ?";
			con.add("%"+sysFileinfo.getRealName()+"%");
		}
		if(StringUtils.isNotEmpty(sysFileinfo.getUploadName())){
			sql+=" and t.upload_name like ?";
			con.add("%"+sysFileinfo.getUploadName()+"%");
		}
		if(sysFileinfo.getFileSize()!=null){
			sql+=" and t.file_size=?";
			con.add(sysFileinfo.getFileSize());
		}
		if(StringUtils.isNotEmpty(sysFileinfo.getFileUrl())){
			sql+=" and t.file_url like ?";
			con.add("%"+sysFileinfo.getFileUrl()+"%");
		}
		if(sysFileinfo.getCreator()!=null){
			sql+=" and t.creator=?";
			con.add(sysFileinfo.getCreator());
		}
		if(sysFileinfo.getCreateTime()!=null){
			sql+=" and t.create_time=?";
			con.add(sysFileinfo.getCreateTime());
		}
		if(sysFileinfo.getCId()!=null){
			sql+=" and t.c_id=?";
			con.add(sysFileinfo.getCId());
		}
		List<SysFileinfo> sysFileinfoList =  jdbcTemplate.queryForListBeanByPage(sql, con, SysFileinfo.class, page);
		
		return sysFileinfoList;
	}
}
