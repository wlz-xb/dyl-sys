<#macro elOut el >${'${'}${el}${'}'}</#macro>
<%@include file="/taglib.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<form class="layui-form" action="${'${'}not empty ${table.javaProperty}?'${table.javaProperty}!update.do':'${table.javaProperty}!add.do'${'}'}"  id="saveOrUpdateForm"><!--编辑表单-->
    <input type="hidden"  name="${table.primaryKey.javaProperty}" value="<@elOut el="${table.javaProperty}.${table.primaryKey.javaProperty}"/>" />
<#list table.baseColumns as c>
  <#if c.javaType !='Date'>
	<div class="layui-form-item">
		<label class="layui-form-label">${c.remarks!c.javaProperty}</label>
		<div class="layui-input-inline">
		  <input type="text" name="${c.javaProperty}" required  lay-verify="required" placeholder="请输入${c.remarks!c.javaProperty}" value="<@elOut el="${table.javaProperty}.${c.javaProperty}"/>" class="layui-input"  />
		</div>
    </div>
  <#else>
	<div class="layui-form-item">
		<label class="layui-form-label">${c.remarks!c.javaProperty}</label>
		<div class="layui-input-inline">
		  <input type="text" name="${c.javaProperty}" required  lay-verify="required" placeholder="请输入${c.remarks!c.javaProperty}" value="<@elOut el="${table.javaProperty}.${c.javaProperty}"/>" class="layui-input"  />
		</div>
    </div>
  </#if>								
</#list>
  <!--隐藏提交按钮，用于触发表单校验 -->
  <button lay-filter="saveOrUpdate" lay-submit class="layui-hide" />
</form>
<script>
	form.render();//重新渲染表单
	form.on('submit(saveOrUpdate)', function(data){//表单提交方法
 		getJsonByajaxForm("saveOrUpdateForm","",function(data){
			if(data.result){
				layer.closeAll('page'); //关闭信息框
				layer.alert($('.layui-layer-btn0').text()+"成功!",function(){
					$('#search').click();//查询
				}); 
			}else{
				l.alert(data.msg);//错误消息弹出
			}
		},true); 
		return false; //阻止表单跳转。如果需要表单跳转，去掉这段即可。									
	}); 
</script>
