/*
Navicat MySQL Data Transfer

Source Server         : dyl
Source Server Version : 50547
Source Host           : localhost:3306
Source Database       : dyl-sys

Target Server Type    : MYSQL
Target Server Version : 50547
File Encoding         : 65001

Date: 2017-06-06 10:51:39
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for sys_auth_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_auth_role`;
CREATE TABLE `sys_auth_role` (
  `id` decimal(10,0) NOT NULL COMMENT '角色id',
  `role_id` decimal(10,0) DEFAULT NULL COMMENT ' 角色id',
  `menu_id` decimal(10,0) DEFAULT NULL COMMENT '菜单id',
  `kind_id` decimal(10,0) DEFAULT NULL COMMENT '权限id',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_auth_role
-- ----------------------------
INSERT INTO `sys_auth_role` VALUES ('100024', '2', '2', '1', '2017-05-27 11:27:37');
INSERT INTO `sys_auth_role` VALUES ('100025', '2', '2', '4', '2017-05-27 11:27:37');
INSERT INTO `sys_auth_role` VALUES ('100026', '2', '3', '1', '2017-05-27 11:27:37');

-- ----------------------------
-- Table structure for sys_kind
-- ----------------------------
DROP TABLE IF EXISTS `sys_kind`;
CREATE TABLE `sys_kind` (
  `id` decimal(10,0) NOT NULL,
  `name` varchar(20) DEFAULT NULL COMMENT '种类名称',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_kind
-- ----------------------------
INSERT INTO `sys_kind` VALUES ('1', '查看');
INSERT INTO `sys_kind` VALUES ('2', '新增');
INSERT INTO `sys_kind` VALUES ('3', '修改');
INSERT INTO `sys_kind` VALUES ('4', '删除');

-- ----------------------------
-- Table structure for sys_log
-- ----------------------------
DROP TABLE IF EXISTS `sys_log`;
CREATE TABLE `sys_log` (
  `id` bigint(20) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `para` varchar(255) DEFAULT NULL,
  `loginUser` bigint(20) DEFAULT NULL COMMENT '登陆用户',
  `ip` varchar(255) DEFAULT NULL COMMENT 'Ip地址',
  `time` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_log
-- ----------------------------
INSERT INTO `sys_log` VALUES ('109114', '/login!intologin.do', '', null, '0:0:0:0:0:0:0:1', '2017-05-26 10:28:34');
INSERT INTO `sys_log` VALUES ('109115', '/login.do', '?username=dev&password=123', null, '0:0:0:0:0:0:0:1', '2017-05-26 10:28:45');
INSERT INTO `sys_log` VALUES ('109116', '/index.do', '', '0', '0:0:0:0:0:0:0:1', '2017-05-26 10:28:46');
INSERT INTO `sys_log` VALUES ('109117', '/main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-05-26 10:28:46');
INSERT INTO `sys_log` VALUES ('109118', '/sysMenu!getMenu.do', '', '0', '0:0:0:0:0:0:0:1', '2017-05-26 10:28:46');
INSERT INTO `sys_log` VALUES ('109119', '/sysUser!main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-05-26 10:28:53');
INSERT INTO `sys_log` VALUES ('109120', '/sysMenu!main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-05-26 10:28:53');
INSERT INTO `sys_log` VALUES ('109121', '/sysMenu!getMenuForZtreeByManage.do', '', '0', '0:0:0:0:0:0:0:1', '2017-05-26 10:28:54');
INSERT INTO `sys_log` VALUES ('109122', '/sysMenu!sysMenuForm.do', '?id=2', '0', '0:0:0:0:0:0:0:1', '2017-05-26 10:28:55');
INSERT INTO `sys_log` VALUES ('109123', '/sysRole!main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-05-26 10:28:59');
INSERT INTO `sys_log` VALUES ('109124', '/sysRole!auth.do', '?roleId=1', '0', '0:0:0:0:0:0:0:1', '2017-05-26 10:29:00');
INSERT INTO `sys_log` VALUES ('109125', '/sysMenu!getMenuForZtree.do', '?roleId=1', '0', '0:0:0:0:0:0:0:1', '2017-05-26 10:29:00');
INSERT INTO `sys_log` VALUES ('109126', '/sysRole!auth.do', '?roleId=1', '0', '0:0:0:0:0:0:0:1', '2017-05-26 10:30:24');
INSERT INTO `sys_log` VALUES ('109127', '/sysMenu!getMenuForZtree.do', '?roleId=1', '0', '0:0:0:0:0:0:0:1', '2017-05-26 10:30:24');
INSERT INTO `sys_log` VALUES ('109128', '/sysRole!auth.do', '?roleId=212', '0', '0:0:0:0:0:0:0:1', '2017-05-26 10:30:28');
INSERT INTO `sys_log` VALUES ('109129', '/sysMenu!getMenuForZtree.do', '?roleId=212', '0', '0:0:0:0:0:0:0:1', '2017-05-26 10:30:28');
INSERT INTO `sys_log` VALUES ('109130', '/sysRole!saveAuth.do', '?authRoleKinds=3_2,4_1,4_2,4_3,4_4&roleId=212', '0', '0:0:0:0:0:0:0:1', '2017-05-26 10:30:34');
INSERT INTO `sys_log` VALUES ('109131', '/sysRole!auth.do', '?roleId=212', '0', '0:0:0:0:0:0:0:1', '2017-05-26 10:30:35');
INSERT INTO `sys_log` VALUES ('109132', '/sysMenu!getMenuForZtree.do', '?roleId=212', '0', '0:0:0:0:0:0:0:1', '2017-05-26 10:30:35');
INSERT INTO `sys_log` VALUES ('109133', '/sysRole!auth.do', '?roleId=212', '0', '0:0:0:0:0:0:0:1', '2017-05-26 10:30:49');
INSERT INTO `sys_log` VALUES ('109134', '/sysMenu!getMenuForZtree.do', '?roleId=212', '0', '0:0:0:0:0:0:0:1', '2017-05-26 10:30:49');
INSERT INTO `sys_log` VALUES ('109135', '/sysRole!auth.do', '?roleId=212', '0', '0:0:0:0:0:0:0:1', '2017-05-26 10:31:22');
INSERT INTO `sys_log` VALUES ('109136', '/sysMenu!getMenuForZtree.do', '?roleId=212', '0', '0:0:0:0:0:0:0:1', '2017-05-26 10:31:22');
INSERT INTO `sys_log` VALUES ('109137', '/sysRole!saveAuth.do', '?authRoleKinds=4_1,4_2,4_3,4_4&roleId=212', '0', '0:0:0:0:0:0:0:1', '2017-05-26 10:31:24');
INSERT INTO `sys_log` VALUES ('109138', '/login!intologin.do', '', null, '0:0:0:0:0:0:0:1', '2017-05-26 11:11:37');
INSERT INTO `sys_log` VALUES ('109139', '/inde.do', '', null, '0:0:0:0:0:0:0:1', '2017-05-26 11:11:44');
INSERT INTO `sys_log` VALUES ('109140', '/login!intologin.do', '', null, '0:0:0:0:0:0:0:1', '2017-05-26 11:11:44');
INSERT INTO `sys_log` VALUES ('109141', '/login.do', '?username=test&password=test', null, '0:0:0:0:0:0:0:1', '2017-05-26 11:11:47');
INSERT INTO `sys_log` VALUES ('109142', '/index.do', '', '211', '0:0:0:0:0:0:0:1', '2017-05-26 11:11:47');
INSERT INTO `sys_log` VALUES ('109143', '/main.do', '', '211', '0:0:0:0:0:0:0:1', '2017-05-26 11:11:47');
INSERT INTO `sys_log` VALUES ('109144', '/sysMenu!getMenu.do', '', '211', '0:0:0:0:0:0:0:1', '2017-05-26 11:11:47');
INSERT INTO `sys_log` VALUES ('109145', '/index.do', '', '211', '0:0:0:0:0:0:0:1', '2017-05-26 11:11:49');
INSERT INTO `sys_log` VALUES ('109146', '/main.do', '', '211', '0:0:0:0:0:0:0:1', '2017-05-26 11:11:49');
INSERT INTO `sys_log` VALUES ('109147', '/sysMenu!getMenu.do', '', '211', '0:0:0:0:0:0:0:1', '2017-05-26 11:11:49');
INSERT INTO `sys_log` VALUES ('109148', '/sysRole!main.do', '', '211', '0:0:0:0:0:0:0:1', '2017-05-26 11:11:51');
INSERT INTO `sys_log` VALUES ('109149', '/login.do', '?username=admin&password=admin', null, '0:0:0:0:0:0:0:1', '2017-05-26 11:11:59');
INSERT INTO `sys_log` VALUES ('109150', '/index.do', '', '2', '0:0:0:0:0:0:0:1', '2017-05-26 11:11:59');
INSERT INTO `sys_log` VALUES ('109151', '/main.do', '', '2', '0:0:0:0:0:0:0:1', '2017-05-26 11:11:59');
INSERT INTO `sys_log` VALUES ('109152', '/sysMenu!getMenu.do', '', '2', '0:0:0:0:0:0:0:1', '2017-05-26 11:11:59');
INSERT INTO `sys_log` VALUES ('109153', '/sysRole!main.do', '', '2', '0:0:0:0:0:0:0:1', '2017-05-26 11:12:00');
INSERT INTO `sys_log` VALUES ('109154', '/sysUser!main.do', '', '2', '0:0:0:0:0:0:0:1', '2017-05-26 11:12:22');
INSERT INTO `sys_log` VALUES ('109155', '/sysRole!sysRoleForm.do', '?id=2', '2', '0:0:0:0:0:0:0:1', '2017-05-26 11:12:28');
INSERT INTO `sys_log` VALUES ('109156', '/sysRole!auth.do', '?roleId=2', '2', '0:0:0:0:0:0:0:1', '2017-05-26 11:12:30');
INSERT INTO `sys_log` VALUES ('109157', '/sysMenu!getMenuForZtree.do', '?roleId=2', '2', '0:0:0:0:0:0:0:1', '2017-05-26 11:12:30');
INSERT INTO `sys_log` VALUES ('109158', '/sysRole!saveAuth.do', '?authRoleKinds=2_1,2_3&roleId=2', '2', '0:0:0:0:0:0:0:1', '2017-05-26 11:12:35');
INSERT INTO `sys_log` VALUES ('109159', '/logout.do', '', '211', '0:0:0:0:0:0:0:1', '2017-05-26 11:13:20');
INSERT INTO `sys_log` VALUES ('109160', '/login.do', '?username=test&password=test', null, '0:0:0:0:0:0:0:1', '2017-05-26 11:13:22');
INSERT INTO `sys_log` VALUES ('109161', '/index.do', '', '211', '0:0:0:0:0:0:0:1', '2017-05-26 11:13:22');
INSERT INTO `sys_log` VALUES ('109162', '/main.do', '', '211', '0:0:0:0:0:0:0:1', '2017-05-26 11:13:22');
INSERT INTO `sys_log` VALUES ('109163', '/sysMenu!getMenu.do', '', '211', '0:0:0:0:0:0:0:1', '2017-05-26 11:13:22');
INSERT INTO `sys_log` VALUES ('109164', '/sysUser!main.do', '', '211', '0:0:0:0:0:0:0:1', '2017-05-26 11:13:23');
INSERT INTO `sys_log` VALUES ('109165', '/sysRole!sysRoleForm.do', '?id=212', '2', '0:0:0:0:0:0:0:1', '2017-05-26 11:13:33');
INSERT INTO `sys_log` VALUES ('109166', '/sysRole!auth.do', '?roleId=212', '2', '0:0:0:0:0:0:0:1', '2017-05-26 11:13:35');
INSERT INTO `sys_log` VALUES ('109167', '/sysMenu!getMenuForZtree.do', '?roleId=212', '2', '0:0:0:0:0:0:0:1', '2017-05-26 11:13:35');
INSERT INTO `sys_log` VALUES ('109168', '/sysRole!saveAuth.do', '?authRoleKinds=2_4,4_1,4_2,4_3,4_4&roleId=212', '2', '0:0:0:0:0:0:0:1', '2017-05-26 11:13:38');
INSERT INTO `sys_log` VALUES ('109169', '/sysUser!main.do', '', '211', '0:0:0:0:0:0:0:1', '2017-05-26 11:13:43');
INSERT INTO `sys_log` VALUES ('109170', '/logout.do', '', '211', '0:0:0:0:0:0:0:1', '2017-05-26 11:13:46');
INSERT INTO `sys_log` VALUES ('109171', '/login.do', '?username=test&password=test', null, '0:0:0:0:0:0:0:1', '2017-05-26 11:13:49');
INSERT INTO `sys_log` VALUES ('109172', '/index.do', '', '211', '0:0:0:0:0:0:0:1', '2017-05-26 11:13:49');
INSERT INTO `sys_log` VALUES ('109173', '/main.do', '', '211', '0:0:0:0:0:0:0:1', '2017-05-26 11:13:49');
INSERT INTO `sys_log` VALUES ('109174', '/sysMenu!getMenu.do', '', '211', '0:0:0:0:0:0:0:1', '2017-05-26 11:13:49');
INSERT INTO `sys_log` VALUES ('109175', '/sysUser!main.do', '', '211', '0:0:0:0:0:0:0:1', '2017-05-26 11:13:50');
INSERT INTO `sys_log` VALUES ('109176', '/sysRole!main.do', '', '211', '0:0:0:0:0:0:0:1', '2017-05-26 11:13:51');
INSERT INTO `sys_log` VALUES ('109177', '/index.do', '', null, '0:0:0:0:0:0:0:1', '2017-05-26 11:19:57');
INSERT INTO `sys_log` VALUES ('109178', '/login!intologin.do', '', null, '0:0:0:0:0:0:0:1', '2017-05-26 11:19:57');
INSERT INTO `sys_log` VALUES ('109179', '/login.do', '?username=test&password=test', null, '0:0:0:0:0:0:0:1', '2017-05-26 11:19:59');
INSERT INTO `sys_log` VALUES ('109180', '/index.do', '', '211', '0:0:0:0:0:0:0:1', '2017-05-26 11:19:59');
INSERT INTO `sys_log` VALUES ('109181', '/main.do', '', '211', '0:0:0:0:0:0:0:1', '2017-05-26 11:19:59');
INSERT INTO `sys_log` VALUES ('109182', '/sysMenu!getMenu.do', '', '211', '0:0:0:0:0:0:0:1', '2017-05-26 11:19:59');
INSERT INTO `sys_log` VALUES ('109183', '/sysUser!main.do', '', '211', '0:0:0:0:0:0:0:1', '2017-05-26 11:20:00');
INSERT INTO `sys_log` VALUES ('109184', '/sysRole!main.do', '', '211', '0:0:0:0:0:0:0:1', '2017-05-26 11:20:02');
INSERT INTO `sys_log` VALUES ('109185', '/logout.do', '', '211', '0:0:0:0:0:0:0:1', '2017-05-26 11:20:09');
INSERT INTO `sys_log` VALUES ('109186', '/login.do', '?username=dev&password=123', null, '0:0:0:0:0:0:0:1', '2017-05-26 11:20:11');
INSERT INTO `sys_log` VALUES ('109187', '/index.do', '', '0', '0:0:0:0:0:0:0:1', '2017-05-26 11:20:11');
INSERT INTO `sys_log` VALUES ('109188', '/main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-05-26 11:20:11');
INSERT INTO `sys_log` VALUES ('109189', '/sysMenu!getMenu.do', '', '0', '0:0:0:0:0:0:0:1', '2017-05-26 11:20:11');
INSERT INTO `sys_log` VALUES ('109190', '/sysMenu!main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-05-26 11:20:12');
INSERT INTO `sys_log` VALUES ('109191', '/sysMenu!getMenuForZtreeByManage.do', '', '0', '0:0:0:0:0:0:0:1', '2017-05-26 11:20:13');
INSERT INTO `sys_log` VALUES ('109192', '/sysQuartz!main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-05-26 11:20:15');
INSERT INTO `sys_log` VALUES ('109193', '/sysQuartz!sysQuartzForm.do', '?id=1', '0', '0:0:0:0:0:0:0:1', '2017-05-26 11:20:16');
INSERT INTO `sys_log` VALUES ('109194', '/sysQuartz!update.do', '?id=1&cronexpression=0/3 * * * * ?&methodname=haha&concurrent=1&state=0&jobdetailname=detailname&targetobject=com.sys.quartz.TestQuarz&triggername=测试1', '0', '0:0:0:0:0:0:0:1', '2017-05-26 11:20:21');
INSERT INTO `sys_log` VALUES ('109195', '/sysQuartz!main.do', '?currentPage=1&triggername=&totalPage=1', '0', '0:0:0:0:0:0:0:1', '2017-05-26 11:20:22');
INSERT INTO `sys_log` VALUES ('109196', '/sysQuartz!main.do', '?currentPage=1&triggername=&totalPage=1', '0', '0:0:0:0:0:0:0:1', '2017-05-26 11:21:19');
INSERT INTO `sys_log` VALUES ('109197', '/logout.do', '', '0', '0:0:0:0:0:0:0:1', '2017-05-26 11:24:01');
INSERT INTO `sys_log` VALUES ('109198', '/login.do', '?username=admin&password=admin', null, '0:0:0:0:0:0:0:1', '2017-05-26 11:24:05');
INSERT INTO `sys_log` VALUES ('109199', '/index.do', '', '2', '0:0:0:0:0:0:0:1', '2017-05-26 11:24:05');
INSERT INTO `sys_log` VALUES ('109200', '/main.do', '', '2', '0:0:0:0:0:0:0:1', '2017-05-26 11:24:05');
INSERT INTO `sys_log` VALUES ('109201', '/sysMenu!getMenu.do', '', '2', '0:0:0:0:0:0:0:1', '2017-05-26 11:24:05');
INSERT INTO `sys_log` VALUES ('109202', '/sysUser!main.do', '', '2', '0:0:0:0:0:0:0:1', '2017-05-26 11:24:06');
INSERT INTO `sys_log` VALUES ('109203', '/sysRole!main.do', '', '2', '0:0:0:0:0:0:0:1', '2017-05-26 11:26:52');
INSERT INTO `sys_log` VALUES ('109204', '/sysRole!main.do', '?name=&currentPage=1&totalPage=1', '2', '0:0:0:0:0:0:0:1', '2017-05-26 11:27:03');
INSERT INTO `sys_log` VALUES ('109205', '/sysRole!main.do', '?name=&currentPage=1&totalPage=1', '2', '0:0:0:0:0:0:0:1', '2017-05-26 11:27:35');
INSERT INTO `sys_log` VALUES ('109206', '/sysRole!auth.do', '?roleId=2', '2', '0:0:0:0:0:0:0:1', '2017-05-26 11:27:36');
INSERT INTO `sys_log` VALUES ('109207', '/sysMenu!getMenuForZtree.do', '?roleId=2', '2', '0:0:0:0:0:0:0:1', '2017-05-26 11:27:36');
INSERT INTO `sys_log` VALUES ('109208', '/sysRole!auth.do', '?roleId=212', '2', '0:0:0:0:0:0:0:1', '2017-05-26 11:27:38');
INSERT INTO `sys_log` VALUES ('109209', '/sysMenu!getMenuForZtree.do', '?roleId=212', '2', '0:0:0:0:0:0:0:1', '2017-05-26 11:27:38');
INSERT INTO `sys_log` VALUES ('109210', '/sysRole!auth.do', '?roleId=212', '2', '0:0:0:0:0:0:0:1', '2017-05-26 11:27:41');
INSERT INTO `sys_log` VALUES ('109211', '/sysMenu!getMenuForZtree.do', '?roleId=212', '2', '0:0:0:0:0:0:0:1', '2017-05-26 11:27:41');
INSERT INTO `sys_log` VALUES ('109212', '/sysRole!auth.do', '?roleId=2', '2', '0:0:0:0:0:0:0:1', '2017-05-26 11:27:45');
INSERT INTO `sys_log` VALUES ('109213', '/sysMenu!getMenuForZtree.do', '?roleId=2', '2', '0:0:0:0:0:0:0:1', '2017-05-26 11:27:45');
INSERT INTO `sys_log` VALUES ('109214', '/sysRole!auth.do', '?roleId=2', '2', '0:0:0:0:0:0:0:1', '2017-05-26 13:35:12');
INSERT INTO `sys_log` VALUES ('109215', '/sysMenu!getMenuForZtree.do', '?roleId=2', '2', '0:0:0:0:0:0:0:1', '2017-05-26 13:35:12');
INSERT INTO `sys_log` VALUES ('109216', '/sysRole!auth.do', '?roleId=2', '2', '0:0:0:0:0:0:0:1', '2017-05-26 13:54:13');
INSERT INTO `sys_log` VALUES ('109217', '/sysMenu!getMenuForZtree.do', '?roleId=2', '2', '0:0:0:0:0:0:0:1', '2017-05-26 13:54:13');
INSERT INTO `sys_log` VALUES ('109218', '/sysRole!auth.do', '?roleId=212', '2', '0:0:0:0:0:0:0:1', '2017-05-26 13:54:20');
INSERT INTO `sys_log` VALUES ('109219', '/sysMenu!getMenuForZtree.do', '?roleId=212', '2', '0:0:0:0:0:0:0:1', '2017-05-26 13:54:20');
INSERT INTO `sys_log` VALUES ('109220', '/sysRole!auth.do', '?roleId=212', '2', '0:0:0:0:0:0:0:1', '2017-05-26 13:54:49');
INSERT INTO `sys_log` VALUES ('109221', '/sysMenu!getMenuForZtree.do', '?roleId=212', '2', '0:0:0:0:0:0:0:1', '2017-05-26 13:54:49');
INSERT INTO `sys_log` VALUES ('109222', '/sysRole!auth.do', '?roleId=2', '2', '0:0:0:0:0:0:0:1', '2017-05-26 13:54:51');
INSERT INTO `sys_log` VALUES ('109223', '/sysMenu!getMenuForZtree.do', '?roleId=2', '2', '0:0:0:0:0:0:0:1', '2017-05-26 13:54:51');
INSERT INTO `sys_log` VALUES ('109224', '/sysRole!saveAuth.do', '?authRoleKinds=2_1,2_3&roleId=2', '2', '0:0:0:0:0:0:0:1', '2017-05-26 13:54:53');
INSERT INTO `sys_log` VALUES ('109225', '/sysRole!auth.do', '?roleId=2', '2', '0:0:0:0:0:0:0:1', '2017-05-26 13:55:01');
INSERT INTO `sys_log` VALUES ('109226', '/sysMenu!getMenuForZtree.do', '?roleId=2', '2', '0:0:0:0:0:0:0:1', '2017-05-26 13:55:01');
INSERT INTO `sys_log` VALUES ('109227', '/sysRole!saveAuth.do', '?authRoleKinds=2_1,2_1,2_3,2_3&roleId=2', '2', '0:0:0:0:0:0:0:1', '2017-05-26 13:55:02');
INSERT INTO `sys_log` VALUES ('109228', '/index.do', '', null, '0:0:0:0:0:0:0:1', '2017-05-26 13:55:34');
INSERT INTO `sys_log` VALUES ('109229', '/login!intologin.do', '', null, '0:0:0:0:0:0:0:1', '2017-05-26 13:55:34');
INSERT INTO `sys_log` VALUES ('109230', '/login.do', '?username=admin&password=admin', null, '0:0:0:0:0:0:0:1', '2017-05-26 13:59:49');
INSERT INTO `sys_log` VALUES ('109231', '/index.do', '', '2', '0:0:0:0:0:0:0:1', '2017-05-26 13:59:49');
INSERT INTO `sys_log` VALUES ('109232', '/main.do', '', '2', '0:0:0:0:0:0:0:1', '2017-05-26 13:59:49');
INSERT INTO `sys_log` VALUES ('109233', '/sysMenu!getMenu.do', '', '2', '0:0:0:0:0:0:0:1', '2017-05-26 13:59:49');
INSERT INTO `sys_log` VALUES ('109234', '/sysRole!main.do', '', '2', '0:0:0:0:0:0:0:1', '2017-05-26 13:59:51');
INSERT INTO `sys_log` VALUES ('109235', '/sysRole!auth.do', '?roleId=212', '2', '0:0:0:0:0:0:0:1', '2017-05-26 13:59:53');
INSERT INTO `sys_log` VALUES ('109236', '/sysMenu!getMenuForZtree.do', '?roleId=212', '2', '0:0:0:0:0:0:0:1', '2017-05-26 13:59:53');
INSERT INTO `sys_log` VALUES ('109237', '/sysRole!auth.do', '?roleId=2', '2', '0:0:0:0:0:0:0:1', '2017-05-26 13:59:55');
INSERT INTO `sys_log` VALUES ('109238', '/sysMenu!getMenuForZtree.do', '?roleId=2', '2', '0:0:0:0:0:0:0:1', '2017-05-26 13:59:55');
INSERT INTO `sys_log` VALUES ('109239', '/sysRole!saveAuth.do', '?authRoleKinds=2_3,2_4&roleId=2', '2', '0:0:0:0:0:0:0:1', '2017-05-26 13:59:57');
INSERT INTO `sys_log` VALUES ('109240', '/sysRole!auth.do', '?roleId=2', '2', '0:0:0:0:0:0:0:1', '2017-05-26 13:59:58');
INSERT INTO `sys_log` VALUES ('109241', '/sysMenu!getMenuForZtree.do', '?roleId=2', '2', '0:0:0:0:0:0:0:1', '2017-05-26 13:59:58');
INSERT INTO `sys_log` VALUES ('109242', '/sysRole!auth.do', '?roleId=2', '2', '0:0:0:0:0:0:0:1', '2017-05-26 14:00:09');
INSERT INTO `sys_log` VALUES ('109243', '/sysMenu!getMenuForZtree.do', '?roleId=2', '2', '0:0:0:0:0:0:0:1', '2017-05-26 14:00:09');
INSERT INTO `sys_log` VALUES ('109244', '/sysRole!saveAuth.do', '?authRoleKinds=2_4&roleId=2', '2', '0:0:0:0:0:0:0:1', '2017-05-26 14:00:12');
INSERT INTO `sys_log` VALUES ('109245', '/sysRole!auth.do', '?roleId=2', '2', '0:0:0:0:0:0:0:1', '2017-05-26 14:00:36');
INSERT INTO `sys_log` VALUES ('109246', '/sysMenu!getMenuForZtree.do', '?roleId=2', '2', '0:0:0:0:0:0:0:1', '2017-05-26 14:00:36');
INSERT INTO `sys_log` VALUES ('109247', '/sysRole!saveAuth.do', '?authRoleKinds=2_4&roleId=2', '2', '0:0:0:0:0:0:0:1', '2017-05-26 14:00:39');
INSERT INTO `sys_log` VALUES ('109248', '/sysRole!auth.do', '?roleId=2', '2', '0:0:0:0:0:0:0:1', '2017-05-26 14:00:58');
INSERT INTO `sys_log` VALUES ('109249', '/sysMenu!getMenuForZtree.do', '?roleId=2', '2', '0:0:0:0:0:0:0:1', '2017-05-26 14:00:58');
INSERT INTO `sys_log` VALUES ('109250', '/sysRole!saveAuth.do', '?authRoleKinds=2_4&roleId=2', '2', '0:0:0:0:0:0:0:1', '2017-05-26 14:01:00');
INSERT INTO `sys_log` VALUES ('109251', '/sysRole!auth.do', '?roleId=2', '2', '0:0:0:0:0:0:0:1', '2017-05-26 14:02:15');
INSERT INTO `sys_log` VALUES ('109252', '/sysMenu!getMenuForZtree.do', '?roleId=2', '2', '0:0:0:0:0:0:0:1', '2017-05-26 14:02:15');
INSERT INTO `sys_log` VALUES ('109253', '/sysRole!saveAuth.do', '?authRoleKinds=2_4,3_1&roleId=2', '2', '0:0:0:0:0:0:0:1', '2017-05-26 14:02:18');
INSERT INTO `sys_log` VALUES ('109254', '/sysRole!auth.do', '?roleId=2', '2', '0:0:0:0:0:0:0:1', '2017-05-26 14:02:19');
INSERT INTO `sys_log` VALUES ('109255', '/sysMenu!getMenuForZtree.do', '?roleId=2', '2', '0:0:0:0:0:0:0:1', '2017-05-26 14:02:19');
INSERT INTO `sys_log` VALUES ('109256', '/sysRole!saveAuth.do', '?authRoleKinds=2_1,2_4,3_1&roleId=2', '2', '0:0:0:0:0:0:0:1', '2017-05-26 14:02:22');
INSERT INTO `sys_log` VALUES ('109257', '/sysRole!auth.do', '?roleId=2', '2', '0:0:0:0:0:0:0:1', '2017-05-26 14:02:23');
INSERT INTO `sys_log` VALUES ('109258', '/sysMenu!getMenuForZtree.do', '?roleId=2', '2', '0:0:0:0:0:0:0:1', '2017-05-26 14:02:23');
INSERT INTO `sys_log` VALUES ('109259', '/index.do', '', '2', '0:0:0:0:0:0:0:1', '2017-05-26 14:09:53');
INSERT INTO `sys_log` VALUES ('109260', '/main.do', '', '2', '0:0:0:0:0:0:0:1', '2017-05-26 14:09:53');
INSERT INTO `sys_log` VALUES ('109261', '/sysMenu!getMenu.do', '', '2', '0:0:0:0:0:0:0:1', '2017-05-26 14:09:53');
INSERT INTO `sys_log` VALUES ('109262', '/sysUser!main.do', '', '2', '0:0:0:0:0:0:0:1', '2017-05-26 14:09:54');
INSERT INTO `sys_log` VALUES ('109263', '/sysRole!main.do', '', '2', '0:0:0:0:0:0:0:1', '2017-05-26 14:09:55');
INSERT INTO `sys_log` VALUES ('109264', '/sysQuartz!main.do', '', '2', '0:0:0:0:0:0:0:1', '2017-05-26 14:09:56');
INSERT INTO `sys_log` VALUES ('109265', '/logout.do', '', '2', '0:0:0:0:0:0:0:1', '2017-05-26 14:10:46');
INSERT INTO `sys_log` VALUES ('109266', '/login.do', '?username=admin&password=admin', null, '0:0:0:0:0:0:0:1', '2017-05-26 14:10:49');
INSERT INTO `sys_log` VALUES ('109267', '/index.do', '', '2', '0:0:0:0:0:0:0:1', '2017-05-26 14:10:49');
INSERT INTO `sys_log` VALUES ('109268', '/main.do', '', '2', '0:0:0:0:0:0:0:1', '2017-05-26 14:10:49');
INSERT INTO `sys_log` VALUES ('109269', '/sysMenu!getMenu.do', '', '2', '0:0:0:0:0:0:0:1', '2017-05-26 14:10:49');
INSERT INTO `sys_log` VALUES ('109270', '/index.do', '', '2', '0:0:0:0:0:0:0:1', '2017-05-26 14:10:51');
INSERT INTO `sys_log` VALUES ('109271', '/main.do', '', '2', '0:0:0:0:0:0:0:1', '2017-05-26 14:10:51');
INSERT INTO `sys_log` VALUES ('109272', '/sysMenu!getMenu.do', '', '2', '0:0:0:0:0:0:0:1', '2017-05-26 14:10:51');
INSERT INTO `sys_log` VALUES ('109273', '/index.do', '', '2', '0:0:0:0:0:0:0:1', '2017-05-26 14:12:33');
INSERT INTO `sys_log` VALUES ('109274', '/main.do', '', '2', '0:0:0:0:0:0:0:1', '2017-05-26 14:12:33');
INSERT INTO `sys_log` VALUES ('109275', '/sysMenu!getMenu.do', '', '2', '0:0:0:0:0:0:0:1', '2017-05-26 14:12:33');
INSERT INTO `sys_log` VALUES ('109276', '/sysRole!main.do', '', '2', '0:0:0:0:0:0:0:1', '2017-05-26 14:12:33');
INSERT INTO `sys_log` VALUES ('109277', '/sysUser!main.do', '', '2', '0:0:0:0:0:0:0:1', '2017-05-26 14:12:34');
INSERT INTO `sys_log` VALUES ('109278', '/index.do', '', '2', '0:0:0:0:0:0:0:1', '2017-05-26 14:13:17');
INSERT INTO `sys_log` VALUES ('109279', '/main.do', '', '2', '0:0:0:0:0:0:0:1', '2017-05-26 14:13:18');
INSERT INTO `sys_log` VALUES ('109280', '/sysMenu!getMenu.do', '', '2', '0:0:0:0:0:0:0:1', '2017-05-26 14:13:18');
INSERT INTO `sys_log` VALUES ('109281', '/index.do', '', '2', '0:0:0:0:0:0:0:1', '2017-05-26 14:13:18');
INSERT INTO `sys_log` VALUES ('109282', '/main.do', '', '2', '0:0:0:0:0:0:0:1', '2017-05-26 14:13:18');
INSERT INTO `sys_log` VALUES ('109283', '/sysMenu!getMenu.do', '', '2', '0:0:0:0:0:0:0:1', '2017-05-26 14:13:18');
INSERT INTO `sys_log` VALUES ('109284', '/logout.do', '', '2', '0:0:0:0:0:0:0:1', '2017-05-26 14:13:21');
INSERT INTO `sys_log` VALUES ('109285', '/login.do', '?username=dev&password=123', null, '0:0:0:0:0:0:0:1', '2017-05-26 14:13:23');
INSERT INTO `sys_log` VALUES ('109286', '/index.do', '', '0', '0:0:0:0:0:0:0:1', '2017-05-26 14:13:23');
INSERT INTO `sys_log` VALUES ('109287', '/main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-05-26 14:13:23');
INSERT INTO `sys_log` VALUES ('109288', '/sysMenu!getMenu.do', '', '0', '0:0:0:0:0:0:0:1', '2017-05-26 14:13:23');
INSERT INTO `sys_log` VALUES ('109289', '/sysRole!main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-05-26 14:13:24');
INSERT INTO `sys_log` VALUES ('109290', '/sysMenu!main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-05-26 14:13:25');
INSERT INTO `sys_log` VALUES ('109291', '/sysMenu!getMenuForZtreeByManage.do', '', '0', '0:0:0:0:0:0:0:1', '2017-05-26 14:13:25');
INSERT INTO `sys_log` VALUES ('109292', '/sysUser!main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-05-26 14:13:25');
INSERT INTO `sys_log` VALUES ('109293', '/sysQuartz!main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-05-26 14:13:27');
INSERT INTO `sys_log` VALUES ('109294', '/sysMenu!sysMenuForm.do', '?id=0', '0', '0:0:0:0:0:0:0:1', '2017-05-26 14:14:06');
INSERT INTO `sys_log` VALUES ('109295', '/sysMenu!main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-05-26 14:14:09');
INSERT INTO `sys_log` VALUES ('109296', '/sysMenu!getMenuForZtreeByManage.do', '', '0', '0:0:0:0:0:0:0:1', '2017-05-26 14:14:09');
INSERT INTO `sys_log` VALUES ('109297', '/sysMenu!main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-05-26 14:14:11');
INSERT INTO `sys_log` VALUES ('109298', '/sysMenu!getMenuForZtreeByManage.do', '', '0', '0:0:0:0:0:0:0:1', '2017-05-26 14:14:11');
INSERT INTO `sys_log` VALUES ('109299', '/sysMenu!main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-05-26 14:14:18');
INSERT INTO `sys_log` VALUES ('109300', '/sysMenu!getMenuForZtreeByManage.do', '', '0', '0:0:0:0:0:0:0:1', '2017-05-26 14:14:18');
INSERT INTO `sys_log` VALUES ('109301', '/sysMenu!main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-05-26 14:14:25');
INSERT INTO `sys_log` VALUES ('109302', '/sysMenu!getMenuForZtreeByManage.do', '', '0', '0:0:0:0:0:0:0:1', '2017-05-26 14:14:25');
INSERT INTO `sys_log` VALUES ('109303', '/sysMenu!sysMenuForm.do', '?id=2', '0', '0:0:0:0:0:0:0:1', '2017-05-26 14:14:26');
INSERT INTO `sys_log` VALUES ('109304', '/sysRole!auth.do', '?roleId=2', '0', '0:0:0:0:0:0:0:1', '2017-05-26 14:14:28');
INSERT INTO `sys_log` VALUES ('109305', '/sysMenu!getMenuForZtree.do', '?roleId=2', '0', '0:0:0:0:0:0:0:1', '2017-05-26 14:14:28');
INSERT INTO `sys_log` VALUES ('109306', '/sysRole!auth.do', '?roleId=212', '0', '0:0:0:0:0:0:0:1', '2017-05-26 14:14:31');
INSERT INTO `sys_log` VALUES ('109307', '/sysMenu!getMenuForZtree.do', '?roleId=212', '0', '0:0:0:0:0:0:0:1', '2017-05-26 14:14:31');
INSERT INTO `sys_log` VALUES ('109308', '/sysMenu!main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-05-26 14:15:04');
INSERT INTO `sys_log` VALUES ('109309', '/sysMenu!getMenuForZtreeByManage.do', '', '0', '0:0:0:0:0:0:0:1', '2017-05-26 14:15:04');
INSERT INTO `sys_log` VALUES ('109310', '/sysMenu!sysMenuForm.do', '?id=2', '0', '0:0:0:0:0:0:0:1', '2017-05-26 14:15:05');
INSERT INTO `sys_log` VALUES ('109311', '/sysMenu!sysMenuForm.do', '?id=3', '0', '0:0:0:0:0:0:0:1', '2017-05-26 14:15:05');
INSERT INTO `sys_log` VALUES ('109312', '/sysMenu!sysMenuForm.do', '?id=3', '0', '0:0:0:0:0:0:0:1', '2017-05-26 14:15:06');
INSERT INTO `sys_log` VALUES ('109313', '/sysMenu!sysMenuForm.do', '?id=4', '0', '0:0:0:0:0:0:0:1', '2017-05-26 14:15:06');
INSERT INTO `sys_log` VALUES ('109314', '/sysMenu!sysMenuForm.do', '?id=241', '0', '0:0:0:0:0:0:0:1', '2017-05-26 14:15:07');
INSERT INTO `sys_log` VALUES ('109315', '/sysMenu!sysMenuForm.do', '?id=264', '0', '0:0:0:0:0:0:0:1', '2017-05-26 14:15:07');
INSERT INTO `sys_log` VALUES ('109316', '/sysMenu!sysMenuForm.do', '?id=241', '0', '0:0:0:0:0:0:0:1', '2017-05-26 14:15:07');
INSERT INTO `sys_log` VALUES ('109317', '/sysMenu!sysMenuForm.do', '?id=3', '0', '0:0:0:0:0:0:0:1', '2017-05-26 14:15:08');
INSERT INTO `sys_log` VALUES ('109318', '/sysMenu!sysMenuForm.do', '?id=1', '0', '0:0:0:0:0:0:0:1', '2017-05-26 14:15:16');
INSERT INTO `sys_log` VALUES ('109319', '/sysMenu!addMenu.do', '?pId=1', '0', '0:0:0:0:0:0:0:1', '2017-05-26 14:15:17');
INSERT INTO `sys_log` VALUES ('109320', '/sysMenu!sysMenuForm.do', '?id=1', '0', '0:0:0:0:0:0:0:1', '2017-05-26 14:15:17');
INSERT INTO `sys_log` VALUES ('109321', '/sysMenu!sysMenuForm.do', '?id=100027', '0', '0:0:0:0:0:0:0:1', '2017-05-26 14:15:17');
INSERT INTO `sys_log` VALUES ('109322', '/sysMenu!update.do', '?id=100027&icon=&title=代码生成器&state=0&actionClass=&url=sys!easyCode.do&note=', '0', '0:0:0:0:0:0:0:1', '2017-05-26 14:15:45');
INSERT INTO `sys_log` VALUES ('109323', '/sysMenu!main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-05-26 14:15:48');
INSERT INTO `sys_log` VALUES ('109324', '/sysMenu!getMenuForZtreeByManage.do', '', '0', '0:0:0:0:0:0:0:1', '2017-05-26 14:15:48');
INSERT INTO `sys_log` VALUES ('109325', '/sysMenu!main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-05-26 14:16:14');
INSERT INTO `sys_log` VALUES ('109326', '/sysMenu!getMenuForZtreeByManage.do', '', '0', '0:0:0:0:0:0:0:1', '2017-05-26 14:16:14');
INSERT INTO `sys_log` VALUES ('109327', '/index.do', '', null, '0:0:0:0:0:0:0:1', '2017-06-05 16:26:17');
INSERT INTO `sys_log` VALUES ('109328', '/login!intologin.do', '', null, '0:0:0:0:0:0:0:1', '2017-06-05 16:26:17');
INSERT INTO `sys_log` VALUES ('109329', '/login.do', '?username=dev&password=123', null, '0:0:0:0:0:0:0:1', '2017-06-05 16:26:19');
INSERT INTO `sys_log` VALUES ('109330', '/index.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-05 16:26:19');
INSERT INTO `sys_log` VALUES ('109331', '/main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-05 16:26:19');
INSERT INTO `sys_log` VALUES ('109332', '/sysMenu!getMenu.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-05 16:26:19');
INSERT INTO `sys_log` VALUES ('109333', '/sysQuartz!main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-05 16:26:20');
INSERT INTO `sys_log` VALUES ('109334', '/sysRole!main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-05 16:26:21');
INSERT INTO `sys_log` VALUES ('109335', '/sysMenu!main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-05 16:26:21');
INSERT INTO `sys_log` VALUES ('109336', '/sysMenu!getMenuForZtreeByManage.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-05 16:26:21');
INSERT INTO `sys_log` VALUES ('109337', '/sysUser!main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-05 16:26:21');
INSERT INTO `sys_log` VALUES ('109338', '/sysMenu!sysMenuForm.do', '?id=2', '0', '0:0:0:0:0:0:0:1', '2017-06-05 16:26:23');
INSERT INTO `sys_log` VALUES ('109339', '/sysMenu!sysMenuForm.do', '?id=3', '0', '0:0:0:0:0:0:0:1', '2017-06-05 16:26:23');
INSERT INTO `sys_log` VALUES ('109340', '/sysMenu!sysMenuForm.do', '?id=4', '0', '0:0:0:0:0:0:0:1', '2017-06-05 16:26:24');
INSERT INTO `sys_log` VALUES ('109341', '/sysMenu!sysMenuForm.do', '?id=6', '0', '0:0:0:0:0:0:0:1', '2017-06-05 16:26:24');
INSERT INTO `sys_log` VALUES ('109342', '/sysMenu!sysMenuForm.do', '?id=7', '0', '0:0:0:0:0:0:0:1', '2017-06-05 16:26:24');
INSERT INTO `sys_log` VALUES ('109343', '/sysMenu!sysMenuForm.do', '?id=5', '0', '0:0:0:0:0:0:0:1', '2017-06-05 16:26:24');
INSERT INTO `sys_log` VALUES ('109344', '/sysMenu!sysMenuForm.do', '?id=5', '0', '0:0:0:0:0:0:0:1', '2017-06-05 16:26:24');
INSERT INTO `sys_log` VALUES ('109345', '/sysMenu!sysMenuForm.do', '?id=8', '0', '0:0:0:0:0:0:0:1', '2017-06-05 16:26:25');
INSERT INTO `sys_log` VALUES ('109346', '/sysMenu!sysMenuForm.do', '?id=5', '0', '0:0:0:0:0:0:0:1', '2017-06-05 16:26:25');
INSERT INTO `sys_log` VALUES ('109347', '/sysMenu!sysMenuForm.do', '?id=6', '0', '0:0:0:0:0:0:0:1', '2017-06-05 16:26:25');
INSERT INTO `sys_log` VALUES ('109348', '/sysMenu!sysMenuForm.do', '?id=4', '0', '0:0:0:0:0:0:0:1', '2017-06-05 16:26:25');
INSERT INTO `sys_log` VALUES ('109349', '/sysMenu!sysMenuForm.do', '?id=3', '0', '0:0:0:0:0:0:0:1', '2017-06-05 16:26:26');
INSERT INTO `sys_log` VALUES ('109350', '/sysMenu!sysMenuForm.do', '?id=2', '0', '0:0:0:0:0:0:0:1', '2017-06-05 16:26:26');
INSERT INTO `sys_log` VALUES ('109351', '/sysMenu!sysMenuForm.do', '?id=6', '0', '0:0:0:0:0:0:0:1', '2017-06-05 16:26:26');
INSERT INTO `sys_log` VALUES ('109352', '/index.do', '', null, '0:0:0:0:0:0:0:1', '2017-06-05 17:33:16');
INSERT INTO `sys_log` VALUES ('109353', '/login!intologin.do', '', null, '0:0:0:0:0:0:0:1', '2017-06-05 17:33:16');
INSERT INTO `sys_log` VALUES ('109354', '/login.do', '?username=dev&password=123', null, '0:0:0:0:0:0:0:1', '2017-06-05 17:33:24');
INSERT INTO `sys_log` VALUES ('109355', '/index.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-05 17:33:24');
INSERT INTO `sys_log` VALUES ('109356', '/main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-05 17:33:24');
INSERT INTO `sys_log` VALUES ('109357', '/sysMenu!getMenu.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-05 17:33:24');
INSERT INTO `sys_log` VALUES ('109358', '/easyCode!main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-05 17:33:25');
INSERT INTO `sys_log` VALUES ('109359', '/easyCode!easyCodeForm.do', '?name=sys_log', '0', '0:0:0:0:0:0:0:1', '2017-06-05 17:33:29');
INSERT INTO `sys_log` VALUES ('109360', '/easyCode.do', '?name=sys_log&pKey=id&templates=javaBean-layui.ftl%40SysLog.java&templates=javaAction-layui.ftl%40SysLogAction.java&templates=javaServiceImpl-layui.ftl%40SysLogServiceImpl.java&templates=viewMain-layui.ftl%40sysLogMain.jsp&templates=viewForm-layui.ftl%40s', '0', '0:0:0:0:0:0:0:1', '2017-06-05 17:33:32');
INSERT INTO `sys_log` VALUES ('109361', '/easyCode.do', '?name=sys_log&pKey=id&templates=javaBean-layui.ftl%40SysLog.java&templates=javaAction-layui.ftl%40SysLogAction.java&templates=javaServiceImpl-layui.ftl%40SysLogServiceImpl.java&templates=viewMain-layui.ftl%40sysLogMain.jsp&templates=viewForm-layui.ftl%40s', '0', '0:0:0:0:0:0:0:1', '2017-06-05 17:36:11');
INSERT INTO `sys_log` VALUES ('109362', '/index.do', '', null, '0:0:0:0:0:0:0:1', '2017-06-05 17:49:43');
INSERT INTO `sys_log` VALUES ('109363', '/login!intologin.do', '', null, '0:0:0:0:0:0:0:1', '2017-06-05 17:49:43');
INSERT INTO `sys_log` VALUES ('109364', '/login.do', '?username=dev&password=12', null, '0:0:0:0:0:0:0:1', '2017-06-05 17:49:45');
INSERT INTO `sys_log` VALUES ('109365', '/login.do', '?username=dev&password=123', null, '0:0:0:0:0:0:0:1', '2017-06-05 17:49:47');
INSERT INTO `sys_log` VALUES ('109366', '/index.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-05 17:49:47');
INSERT INTO `sys_log` VALUES ('109367', '/main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-05 17:49:47');
INSERT INTO `sys_log` VALUES ('109368', '/sysMenu!getMenu.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-05 17:49:47');
INSERT INTO `sys_log` VALUES ('109369', '/sysMenu!main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-05 17:49:49');
INSERT INTO `sys_log` VALUES ('109370', '/sysMenu!getMenuForZtreeByManage.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-05 17:49:49');
INSERT INTO `sys_log` VALUES ('109371', '/sysMenu!addMenu.do', '?pId=1', '0', '0:0:0:0:0:0:0:1', '2017-06-05 17:49:50');
INSERT INTO `sys_log` VALUES ('109372', '/sysMenu!sysMenuForm.do', '?id=1', '0', '0:0:0:0:0:0:0:1', '2017-06-05 17:49:50');
INSERT INTO `sys_log` VALUES ('109373', '/sysMenu!sysMenuForm.do', '?id=100068', '0', '0:0:0:0:0:0:0:1', '2017-06-05 17:49:50');
INSERT INTO `sys_log` VALUES ('109374', '/sysMenu!sysMenuForm.do', '?id=100068', '0', '0:0:0:0:0:0:0:1', '2017-06-05 17:49:51');
INSERT INTO `sys_log` VALUES ('109375', '/sysMenu!sysMenuForm.do', '?id=8', '0', '0:0:0:0:0:0:0:1', '2017-06-05 17:49:52');
INSERT INTO `sys_log` VALUES ('109376', '/sysMenu!sysMenuForm.do', '?id=100068', '0', '0:0:0:0:0:0:0:1', '2017-06-05 17:49:52');
INSERT INTO `sys_log` VALUES ('109377', '/sysMenu!update.do', '?id=100068&icon=&title=系统访问日志&state=0&actionClass=&url=sysLog!main.do&note=', '0', '0:0:0:0:0:0:0:1', '2017-06-05 17:49:57');
INSERT INTO `sys_log` VALUES ('109378', '/log!main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-05 17:50:00');
INSERT INTO `sys_log` VALUES ('109379', '/index.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-05 17:50:01');
INSERT INTO `sys_log` VALUES ('109380', '/main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-05 17:50:01');
INSERT INTO `sys_log` VALUES ('109381', '/sysMenu!getMenu.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-05 17:50:01');
INSERT INTO `sys_log` VALUES ('109382', '/sysLog!main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-05 17:50:02');
INSERT INTO `sys_log` VALUES ('109383', '/sysLog!main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-05 17:50:25');
INSERT INTO `sys_log` VALUES ('109384', '/log!main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-05 17:51:01');
INSERT INTO `sys_log` VALUES ('109385', '/easyCode!main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-05 17:51:02');
INSERT INTO `sys_log` VALUES ('109386', '/sysQuartz!main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-05 17:51:15');
INSERT INTO `sys_log` VALUES ('109387', '/sysQuartz!sysQuartzForm.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-05 17:51:16');
INSERT INTO `sys_log` VALUES ('109388', '/sysQuartz!sysQuartzForm.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-05 17:51:56');
INSERT INTO `sys_log` VALUES ('109389', '/sysQuartz!add.do', '?id=&cronexpression=0/30 * * * * ?&methodname=execute&concurrent=1&state=1&jobdetailname=logInfo&targetobject=dyl.sys.quartz.LogQuarz&triggername=系统访问日志定时器', '0', '0:0:0:0:0:0:0:1', '2017-06-05 17:52:40');
INSERT INTO `sys_log` VALUES ('109390', '/sysQuartz!main.do', '?currentPage=1&triggername=&totalPage=1', '0', '0:0:0:0:0:0:0:1', '2017-06-05 17:52:41');
INSERT INTO `sys_log` VALUES ('109391', '/index.do', '', null, '0:0:0:0:0:0:0:1', '2017-06-05 18:16:25');
INSERT INTO `sys_log` VALUES ('109392', '/login!intologin.do', '', null, '0:0:0:0:0:0:0:1', '2017-06-05 18:16:25');
INSERT INTO `sys_log` VALUES ('109393', '/login.do', '?username=dev&password=123', null, '0:0:0:0:0:0:0:1', '2017-06-05 18:16:26');
INSERT INTO `sys_log` VALUES ('109394', '/index.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-05 18:16:27');
INSERT INTO `sys_log` VALUES ('109395', '/main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-05 18:16:27');
INSERT INTO `sys_log` VALUES ('109396', '/sysMenu!getMenu.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-05 18:16:27');
INSERT INTO `sys_log` VALUES ('109397', '/log!main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-05 18:16:28');
INSERT INTO `sys_log` VALUES ('109398', '/easyCode!main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-05 18:16:28');
INSERT INTO `sys_log` VALUES ('109399', '/sysQuartz!main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-05 18:16:29');
INSERT INTO `sys_log` VALUES ('109400', '/sysLog!main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-05 18:16:40');
INSERT INTO `sys_log` VALUES ('109401', '/sysQuartz!sysQuartzForm.do', '?id=1', '0', '0:0:0:0:0:0:0:1', '2017-06-05 18:16:44');
INSERT INTO `sys_log` VALUES ('109402', '/sysQuartz!sysQuartzForm.do', '?id=1', '0', '0:0:0:0:0:0:0:1', '2017-06-05 18:16:48');
INSERT INTO `sys_log` VALUES ('109403', '/sysQuartz!update.do', '?id=1&cronexpression=0/3 * * * * ?&methodname=haha&concurrent=1&state=1&jobdetailname=detailname&targetobject=testQuarz&triggername=测试', '0', '0:0:0:0:0:0:0:1', '2017-06-05 18:16:53');
INSERT INTO `sys_log` VALUES ('109404', '/sysQuartz!main.do', '?currentPage=1&triggername=&totalPage=1', '0', '0:0:0:0:0:0:0:1', '2017-06-05 18:16:53');
INSERT INTO `sys_log` VALUES ('109405', '/sysQuartz!sysQuartzForm.do', '?id=100069', '0', '0:0:0:0:0:0:0:1', '2017-06-05 18:16:54');
INSERT INTO `sys_log` VALUES ('109406', '/sysQuartz!update.do', '?id=100069&cronexpression=0/3 * * * * ?&methodname=execute&concurrent=1&state=0&jobdetailname=logInfo&targetobject=logQuarz&triggername=系统访问日志定时器', '0', '0:0:0:0:0:0:0:1', '2017-06-05 18:16:58');
INSERT INTO `sys_log` VALUES ('109407', '/sysQuartz!main.do', '?currentPage=1&triggername=&totalPage=1', '0', '0:0:0:0:0:0:0:1', '2017-06-05 18:16:59');
INSERT INTO `sys_log` VALUES ('109408', '/sysQuartz!main.do', '?currentPage=1&triggername=&totalPage=1', '0', '0:0:0:0:0:0:0:1', '2017-06-05 18:17:00');
INSERT INTO `sys_log` VALUES ('109409', '/login!intologin.do', '', null, '0:0:0:0:0:0:0:1', '2017-06-06 10:01:27');
INSERT INTO `sys_log` VALUES ('109410', '/login.do', '?username=dev&password=123', null, '0:0:0:0:0:0:0:1', '2017-06-06 10:01:35');
INSERT INTO `sys_log` VALUES ('109411', '/index.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:01:35');
INSERT INTO `sys_log` VALUES ('109412', '/main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:01:35');
INSERT INTO `sys_log` VALUES ('109413', '/sysMenu!getMenu.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:01:35');
INSERT INTO `sys_log` VALUES ('109414', '/sysMenu!main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:01:36');
INSERT INTO `sys_log` VALUES ('109415', '/sysMenu!getMenuForZtreeByManage.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:01:36');
INSERT INTO `sys_log` VALUES ('109416', '/sysUser!main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:01:37');
INSERT INTO `sys_log` VALUES ('109417', '/log!main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:01:40');
INSERT INTO `sys_log` VALUES ('109418', '/sysLog!main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:01:41');
INSERT INTO `sys_log` VALUES ('109419', '/sysLog!main.do', '?currentPage=4&totalPage=576&url=', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:01:53');
INSERT INTO `sys_log` VALUES ('109420', '/sysLog!main.do', '?currentPage=5&totalPage=576&url=', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:01:54');
INSERT INTO `sys_log` VALUES ('109421', '/sysLog!main.do', '?currentPage=3&totalPage=576&url=', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:01:56');
INSERT INTO `sys_log` VALUES ('109422', '/sysLog!main.do', '?currentPage=4&totalPage=576&url=', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:01:57');
INSERT INTO `sys_log` VALUES ('109423', '/sysLog!main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:03:17');
INSERT INTO `sys_log` VALUES ('109424', '/sysLog!main.do', '', null, '0:0:0:0:0:0:0:1', '2017-06-06 10:06:25');
INSERT INTO `sys_log` VALUES ('109425', '/login!intologin.do', '', null, '0:0:0:0:0:0:0:1', '2017-06-06 10:06:25');
INSERT INTO `sys_log` VALUES ('109426', '/login.do', '?username=dev&password=123', null, '0:0:0:0:0:0:0:1', '2017-06-06 10:06:27');
INSERT INTO `sys_log` VALUES ('109427', '/index.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:06:27');
INSERT INTO `sys_log` VALUES ('109428', '/main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:06:27');
INSERT INTO `sys_log` VALUES ('109429', '/sysMenu!getMenu.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:06:28');
INSERT INTO `sys_log` VALUES ('109430', '/sysLog!main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:06:28');
INSERT INTO `sys_log` VALUES ('109431', '/sysLog!main.do', '?currentPage=1&totalPage=26&url=', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:06:30');
INSERT INTO `sys_log` VALUES ('109432', '/easyCode!main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:06:36');
INSERT INTO `sys_log` VALUES ('109433', '/sysMenu!sysMenuForm.do', '?id=2', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:06:38');
INSERT INTO `sys_log` VALUES ('109434', '/sysMenu!sysMenuForm.do', '?id=3', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:06:38');
INSERT INTO `sys_log` VALUES ('109435', '/sysMenu!sysMenuForm.do', '?id=4', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:06:39');
INSERT INTO `sys_log` VALUES ('109436', '/sysMenu!sysMenuForm.do', '?id=6', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:06:39');
INSERT INTO `sys_log` VALUES ('109437', '/sysMenu!sysMenuForm.do', '?id=5', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:06:39');
INSERT INTO `sys_log` VALUES ('109438', '/sysMenu!sysMenuForm.do', '?id=8', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:06:40');
INSERT INTO `sys_log` VALUES ('109439', '/sysMenu!sysMenuForm.do', '?id=8', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:06:40');
INSERT INTO `sys_log` VALUES ('109440', '/sysMenu!sysMenuForm.do', '?id=100068', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:06:40');
INSERT INTO `sys_log` VALUES ('109441', '/sysMenu!sysMenuForm.do', '?id=8', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:06:40');
INSERT INTO `sys_log` VALUES ('109442', '/sysMenu!sysMenuForm.do', '?id=5', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:06:40');
INSERT INTO `sys_log` VALUES ('109443', '/sysMenu!sysMenuForm.do', '?id=7', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:06:41');
INSERT INTO `sys_log` VALUES ('109444', '/sysMenu!sysMenuForm.do', '?id=6', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:06:41');
INSERT INTO `sys_log` VALUES ('109445', '/sysMenu!sysMenuForm.do', '?id=4', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:06:41');
INSERT INTO `sys_log` VALUES ('109446', '/sysMenu!sysMenuForm.do', '?id=3', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:06:41');
INSERT INTO `sys_log` VALUES ('109447', '/sysLog!main.do', '?currentPage=1&totalPage=27&url=', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:06:44');
INSERT INTO `sys_log` VALUES ('109448', '/sysLog!main.do', '?currentPage=1&totalPage=27&url=', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:06:45');
INSERT INTO `sys_log` VALUES ('109449', '/sysLog!main.do', '?currentPage=1&totalPage=27&url=', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:06:50');
INSERT INTO `sys_log` VALUES ('109450', '/sysLog!main.do', '?currentPage=1&totalPage=27&url=', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:06:50');
INSERT INTO `sys_log` VALUES ('109451', '/sysLog!main.do', '?currentPage=1&totalPage=27&url=', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:07:11');
INSERT INTO `sys_log` VALUES ('109452', '/sysLog!main.do', '?currentPage=1&totalPage=29&url=', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:07:13');
INSERT INTO `sys_log` VALUES ('109453', '/sysLog!main.do', '?currentPage=1&totalPage=29&url=', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:07:14');
INSERT INTO `sys_log` VALUES ('109454', '/sysMenu!sysMenuForm.do', '?id=100068', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:08:07');
INSERT INTO `sys_log` VALUES ('109455', '/sysMenu!update.do', '?id=100068&icon=fa-eercast&title=系统访问日志&state=0&actionClass=&url=sysLog!main.do&note=', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:08:28');
INSERT INTO `sys_log` VALUES ('109456', '/index.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:08:29');
INSERT INTO `sys_log` VALUES ('109457', '/main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:08:29');
INSERT INTO `sys_log` VALUES ('109458', '/sysMenu!getMenu.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:08:30');
INSERT INTO `sys_log` VALUES ('109459', '/sysLog!main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:08:31');
INSERT INTO `sys_log` VALUES ('109460', '/log!main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:08:31');
INSERT INTO `sys_log` VALUES ('109461', '/easyCode!main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:08:31');
INSERT INTO `sys_log` VALUES ('109462', '/sysLog!main.do', '?currentPage=2&totalPage=29&url=', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:08:47');
INSERT INTO `sys_log` VALUES ('109463', '/sysLog!main.do', '?currentPage=1&totalPage=29&url=', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:08:48');
INSERT INTO `sys_log` VALUES ('109464', '/sysLog!main.do', '?currentPage=5&totalPage=29&url=', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:08:48');
INSERT INTO `sys_log` VALUES ('109465', '/sysLog!main.do', '?currentPage=4&totalPage=29&url=', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:08:49');
INSERT INTO `sys_log` VALUES ('109466', '/sysLog!main.do', '?currentPage=5&totalPage=29&url=', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:08:50');
INSERT INTO `sys_log` VALUES ('109467', '/sysLog!main.do', '?currentPage=6&totalPage=29&url=', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:08:51');
INSERT INTO `sys_log` VALUES ('109468', '/sysLog!main.do', '?currentPage=8&totalPage=29&url=', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:08:52');
INSERT INTO `sys_log` VALUES ('109469', '/sysLog!main.do', '?currentPage=10&totalPage=29&url=', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:08:52');
INSERT INTO `sys_log` VALUES ('109470', '/sysLog!main.do', '?currentPage=11&totalPage=29&url=', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:08:53');
INSERT INTO `sys_log` VALUES ('109471', '/sysLog!main.do', '?currentPage=10&totalPage=29&url=', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:08:54');
INSERT INTO `sys_log` VALUES ('109472', '/sysLog!main.do', '?currentPage=8&totalPage=29&url=', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:08:55');
INSERT INTO `sys_log` VALUES ('109473', '/sysLog!main.do', '?currentPage=7&totalPage=29&url=', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:08:55');
INSERT INTO `sys_log` VALUES ('109474', '/sysLog!main.do', '?currentPage=8&totalPage=29&url=', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:08:57');
INSERT INTO `sys_log` VALUES ('109475', '/sysLog!main.do', '?currentPage=10&totalPage=29&url=', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:08:58');
INSERT INTO `sys_log` VALUES ('109476', '/sysLog!main.do', '?currentPage=11&totalPage=29&url=', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:08:59');
INSERT INTO `sys_log` VALUES ('109477', '/sysLog!main.do', '?currentPage=12&totalPage=29&url=', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:08:59');
INSERT INTO `sys_log` VALUES ('109478', '/sysLog!main.do', '?currentPage=29&totalPage=29&url=', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:09:00');
INSERT INTO `sys_log` VALUES ('109479', '/sysLog!main.do', '?currentPage=30&totalPage=31&url=', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:09:02');
INSERT INTO `sys_log` VALUES ('109480', '/sysLog!main.do', '?currentPage=31&totalPage=31&url=', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:09:03');
INSERT INTO `sys_log` VALUES ('109481', '/sysLog!main.do', '?currentPage=29&totalPage=31&url=', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:09:05');
INSERT INTO `sys_log` VALUES ('109482', '/sysLog!main.do', '?currentPage=30&totalPage=31&url=', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:09:05');
INSERT INTO `sys_log` VALUES ('109483', '/sysLog!main.do', '?currentPage=30&totalPage=31&url=sys', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:09:11');
INSERT INTO `sys_log` VALUES ('109484', '/sysQuartz!main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:09:21');
INSERT INTO `sys_log` VALUES ('109485', '/sysRole!main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:10:54');
INSERT INTO `sys_log` VALUES ('109486', '/sysMenu!main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:10:54');
INSERT INTO `sys_log` VALUES ('109487', '/sysMenu!getMenuForZtreeByManage.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:10:54');
INSERT INTO `sys_log` VALUES ('109488', '/sysUser!main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:10:54');
INSERT INTO `sys_log` VALUES ('109489', '/sysLog!main.do', '?currentPage=21&totalPage=22&url=sys', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:10:58');
INSERT INTO `sys_log` VALUES ('109490', '/sysLog!main.do', '?currentPage=20&totalPage=23&url=sys', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:10:59');
INSERT INTO `sys_log` VALUES ('109491', '/sysLog!main.do', '?currentPage=18&totalPage=23&url=sys', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:10:59');
INSERT INTO `sys_log` VALUES ('109492', '/sysLog!main.do', '?currentPage=16&totalPage=23&url=sys', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:11:00');
INSERT INTO `sys_log` VALUES ('109493', '/sysLog!main.do', '?currentPage=14&totalPage=23&url=sys', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:11:02');
INSERT INTO `sys_log` VALUES ('109494', '/sysMenu!sysMenuForm.do', '?id=8', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:13:59');
INSERT INTO `sys_log` VALUES ('109495', '/sysLog!main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:14:27');
INSERT INTO `sys_log` VALUES ('109496', '/log!main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:14:29');
INSERT INTO `sys_log` VALUES ('109497', '/sysLog!main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:14:41');
INSERT INTO `sys_log` VALUES ('109498', '/log!main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:14:42');
INSERT INTO `sys_log` VALUES ('109499', '/sysLog!main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:15:01');
INSERT INTO `sys_log` VALUES ('109500', '/log!main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:15:02');
INSERT INTO `sys_log` VALUES ('109501', '/sysLog!main.do', '?currentPage=1&totalPage=33&url=', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:15:14');
INSERT INTO `sys_log` VALUES ('109502', '/log!main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:15:15');
INSERT INTO `sys_log` VALUES ('109503', '/log!download.do', '?filePath=E:\\workSpace\\.metadata\\.plugins\\org.eclipse.wst.server.core\\tmp1\\logs\\log4j\\logInfo.log&fileName=logInfo.log&filePath=E:\\workSpace\\.metadata\\.plugins\\org.eclipse.wst.server.core\\tmp1\\logs\\log4j\\logInfo.log&fileName=logInfo.log', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:15:21');
INSERT INTO `sys_log` VALUES ('109504', '/sysMenu!sysMenuForm.do', '?id=8', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:15:27');
INSERT INTO `sys_log` VALUES ('109505', '/sysMenu!sysMenuForm.do', '?id=8', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:15:28');
INSERT INTO `sys_log` VALUES ('109506', '/sysMenu!sysMenuForm.do', '?id=5', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:15:28');
INSERT INTO `sys_log` VALUES ('109507', '/sysMenu!sysMenuForm.do', '?id=5', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:15:29');
INSERT INTO `sys_log` VALUES ('109508', '/sysMenu!sysMenuForm.do', '?id=7', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:15:30');
INSERT INTO `sys_log` VALUES ('109509', '/sysMenu!sysMenuForm.do', '?id=6', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:15:30');
INSERT INTO `sys_log` VALUES ('109510', '/sysMenu!sysMenuForm.do', '?id=4', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:15:30');
INSERT INTO `sys_log` VALUES ('109511', '/sysMenu!sysMenuForm.do', '?id=3', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:15:31');
INSERT INTO `sys_log` VALUES ('109512', '/sysMenu!sysMenuForm.do', '?id=2', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:15:31');
INSERT INTO `sys_log` VALUES ('109513', '/sysMenu!sysMenuForm.do', '?id=5', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:15:31');
INSERT INTO `sys_log` VALUES ('109514', '/sysMenu!sysMenuForm.do', '?id=8', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:15:32');
INSERT INTO `sys_log` VALUES ('109515', '/sysMenu!sysMenuForm.do', '?id=100068', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:15:32');
INSERT INTO `sys_log` VALUES ('109516', '/sysMenu!sysMenuForm.do', '?id=8', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:15:33');
INSERT INTO `sys_log` VALUES ('109517', '/sysMenu!sysMenuForm.do', '?id=100068', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:15:33');
INSERT INTO `sys_log` VALUES ('109518', '/sysMenu!delete.do', '?id=100068', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:15:42');
INSERT INTO `sys_log` VALUES ('109519', '/sysMenu!sysMenuForm.do', '?id=8', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:15:43');
INSERT INTO `sys_log` VALUES ('109520', '/sysMenu!update.do', '?id=8&icon=fa-book&title=日志管理&state=0&actionClass=&url=sysLog!main.do&note=', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:15:46');
INSERT INTO `sys_log` VALUES ('109521', '/index.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:15:49');
INSERT INTO `sys_log` VALUES ('109522', '/main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:15:49');
INSERT INTO `sys_log` VALUES ('109523', '/sysMenu!getMenu.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:15:49');
INSERT INTO `sys_log` VALUES ('109524', '/sysLog!main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:15:50');
INSERT INTO `sys_log` VALUES ('109525', '/index.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:15:54');
INSERT INTO `sys_log` VALUES ('109526', '/main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:15:54');
INSERT INTO `sys_log` VALUES ('109527', '/sysMenu!getMenu.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:15:55');
INSERT INTO `sys_log` VALUES ('109528', '/index.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:15:55');
INSERT INTO `sys_log` VALUES ('109529', '/main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:15:55');
INSERT INTO `sys_log` VALUES ('109530', '/sysMenu!getMenu.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:15:56');
INSERT INTO `sys_log` VALUES ('109531', '/sysLog!main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:15:56');
INSERT INTO `sys_log` VALUES ('109532', '/log!main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:16:00');
INSERT INTO `sys_log` VALUES ('109533', '/sysLog!main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:16:51');
INSERT INTO `sys_log` VALUES ('109534', '/sysLog!main.do', '?currentPage=2&totalPage=35&url=', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:16:56');
INSERT INTO `sys_log` VALUES ('109535', '/sysLog!main.do', '?currentPage=3&totalPage=35&url=', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:17:02');
INSERT INTO `sys_log` VALUES ('109536', '/sysLog!main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:18:35');
INSERT INTO `sys_log` VALUES ('109537', '/sysLog!main.do', '?currentPage=2&totalPage=36&url=', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:18:40');
INSERT INTO `sys_log` VALUES ('109538', '/sysLog!main.do', '?currentPage=3&totalPage=36&url=', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:18:42');
INSERT INTO `sys_log` VALUES ('109539', '/index.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:18:45');
INSERT INTO `sys_log` VALUES ('109540', '/main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:18:45');
INSERT INTO `sys_log` VALUES ('109541', '/sysMenu!getMenu.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:18:46');
INSERT INTO `sys_log` VALUES ('109542', '/sysLog!main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:18:47');
INSERT INTO `sys_log` VALUES ('109543', '/sysLog!main.do', '?currentPage=2&totalPage=36&url=', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:18:48');
INSERT INTO `sys_log` VALUES ('109544', '/sysLog!main.do', '?currentPage=3&totalPage=36&url=', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:18:50');
INSERT INTO `sys_log` VALUES ('109545', '/sysLog!main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:24:10');
INSERT INTO `sys_log` VALUES ('109546', '/sysLog!main.do', '?currentPage=2&totalPage=36&url=', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:24:15');
INSERT INTO `sys_log` VALUES ('109547', '/sysLog!main.do', '?currentPage=3&totalPage=36&url=', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:24:17');
INSERT INTO `sys_log` VALUES ('109548', '/sysLog!main.do', '?currentPage=4&totalPage=36&url=', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:24:18');
INSERT INTO `sys_log` VALUES ('109549', '/sysLog!main.do', '?currentPage=5&totalPage=36&url=', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:24:35');
INSERT INTO `sys_log` VALUES ('109550', '/sysLog!main.do', '?currentPage=6&totalPage=37&url=', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:24:37');
INSERT INTO `sys_log` VALUES ('109551', '/sysLog!main.do', '?currentPage=7&totalPage=37&url=', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:24:39');
INSERT INTO `sys_log` VALUES ('109552', '/sysLog!main.do', '?currentPage=8&totalPage=37&url=', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:24:43');
INSERT INTO `sys_log` VALUES ('109553', '/sysLog!main.do', '?currentPage=9&totalPage=37&url=', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:24:45');
INSERT INTO `sys_log` VALUES ('109554', '/sysLog!main.do', '?currentPage=10&totalPage=37&url=', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:24:46');
INSERT INTO `sys_log` VALUES ('109555', '/sysLog!main.do', '?currentPage=12&totalPage=37&url=', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:24:47');
INSERT INTO `sys_log` VALUES ('109556', '/sysLog!main.do', '?currentPage=14&totalPage=37&url=', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:24:52');
INSERT INTO `sys_log` VALUES ('109557', '/sysLog!main.do', '?currentPage=16&totalPage=37&url=', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:24:54');
INSERT INTO `sys_log` VALUES ('109558', '/sysLog!main.do', '?currentPage=18&totalPage=37&url=', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:24:54');
INSERT INTO `sys_log` VALUES ('109559', '/sysLog!main.do', '?currentPage=20&totalPage=37&url=', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:24:55');
INSERT INTO `sys_log` VALUES ('109560', '/sysLog!main.do', '?currentPage=21&totalPage=37&url=', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:24:56');
INSERT INTO `sys_log` VALUES ('109561', '/sysLog!main.do', '?currentPage=23&totalPage=37&url=', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:24:57');
INSERT INTO `sys_log` VALUES ('109562', '/sysLog!main.do', '?currentPage=25&totalPage=37&url=', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:24:57');
INSERT INTO `sys_log` VALUES ('109563', '/sysLog!main.do', '?currentPage=27&totalPage=37&url=', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:24:57');
INSERT INTO `sys_log` VALUES ('109564', '/sysLog!main.do', '?currentPage=29&totalPage=37&url=', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:24:58');
INSERT INTO `sys_log` VALUES ('109565', '/sysLog!main.do', '?currentPage=31&totalPage=37&url=', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:24:58');
INSERT INTO `sys_log` VALUES ('109566', '/sysLog!main.do', '?currentPage=33&totalPage=37&url=', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:24:59');
INSERT INTO `sys_log` VALUES ('109567', '/sysLog!main.do', '?currentPage=35&totalPage=37&url=', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:24:59');
INSERT INTO `sys_log` VALUES ('109568', '/sysLog!main.do', '?currentPage=37&totalPage=37&url=', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:24:59');
INSERT INTO `sys_log` VALUES ('109569', '/sysQuartz!main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:25:08');
INSERT INTO `sys_log` VALUES ('109570', '/easyCode!main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:25:09');
INSERT INTO `sys_log` VALUES ('109571', '/sysLog!main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:25:10');
INSERT INTO `sys_log` VALUES ('109572', '/sysLog!main.do', '?currentPage=3&totalPage=38&url=', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:25:15');
INSERT INTO `sys_log` VALUES ('109573', '/sysLog!main.do', '?currentPage=4&totalPage=38&url=', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:25:16');
INSERT INTO `sys_log` VALUES ('109574', '/sysLog!main.do', '?currentPage=5&totalPage=38&url=', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:25:17');
INSERT INTO `sys_log` VALUES ('109575', '/sysLog!main.do', '?currentPage=6&totalPage=38&url=', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:25:18');
INSERT INTO `sys_log` VALUES ('109576', '/sysLog!main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:27:20');
INSERT INTO `sys_log` VALUES ('109577', '/sysLog!main.do', '?currentPage=4&totalPage=39&url=', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:27:23');
INSERT INTO `sys_log` VALUES ('109578', '/sysLog!main.do', '?currentPage=5&totalPage=39&url=', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:27:24');
INSERT INTO `sys_log` VALUES ('109579', '/sysLog!main.do', '?currentPage=5&totalPage=39&url=', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:27:46');
INSERT INTO `sys_log` VALUES ('109580', '/sysLog!main.do', '?currentPage=5&totalPage=39&url=', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:27:47');
INSERT INTO `sys_log` VALUES ('109581', '/sysLog!main.do', '?currentPage=5&totalPage=39&url=', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:28:14');
INSERT INTO `sys_log` VALUES ('109582', '/sysLog!main.do', '?currentPage=6&totalPage=39&url=', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:28:17');
INSERT INTO `sys_log` VALUES ('109583', '/sysLog!main.do', '?currentPage=7&totalPage=39&url=', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:28:20');
INSERT INTO `sys_log` VALUES ('109584', '/log!main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:28:41');
INSERT INTO `sys_log` VALUES ('109585', '/log!main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:28:47');
INSERT INTO `sys_log` VALUES ('109586', '/sysLog!main.do', '?currentPage=8&totalPage=39&url=', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:28:56');
INSERT INTO `sys_log` VALUES ('109587', '/log!main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:28:58');
INSERT INTO `sys_log` VALUES ('109588', '/sysRole!main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:29:28');
INSERT INTO `sys_log` VALUES ('109589', '/sysMenu!main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:29:28');
INSERT INTO `sys_log` VALUES ('109590', '/sysMenu!getMenuForZtreeByManage.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:29:28');
INSERT INTO `sys_log` VALUES ('109591', '/sysUser!main.do', '', '0', '0:0:0:0:0:0:0:1', '2017-06-06 10:29:29');

-- ----------------------------
-- Table structure for sys_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_menu`;
CREATE TABLE `sys_menu` (
  `id` decimal(10,0) NOT NULL,
  `pid` decimal(10,0) DEFAULT NULL COMMENT '父Id',
  `oid` decimal(10,0) DEFAULT NULL COMMENT '排序',
  `title` varchar(50) DEFAULT NULL COMMENT '名称',
  `note` varchar(100) DEFAULT NULL COMMENT '说明',
  `state` char(1) DEFAULT '0' COMMENT '状态',
  `url` varchar(100) DEFAULT NULL COMMENT '地址',
  `icon` varchar(100) DEFAULT NULL COMMENT '图片代码',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `creator` decimal(10,0) DEFAULT NULL,
  `action_class` varchar(50) DEFAULT NULL,
  `view_level` decimal(10,0) DEFAULT '5' COMMENT '查看等级',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_menu
-- ----------------------------
INSERT INTO `sys_menu` VALUES ('1', '0', '1', '系统管理', null, '0', null, 'fa-cubes', '2017-05-27 11:24:12', '0', null, '2');
INSERT INTO `sys_menu` VALUES ('2', '1', '-1', '用户管理', '', '0', 'sysUser!main.do', '&#xe612;', '2017-06-01 15:41:26', '0', 'sysUserAction', '2');
INSERT INTO `sys_menu` VALUES ('3', '1', '0', '菜单管理', '', '0', 'sysMenu!main.do', '&#xe62a;', '2017-06-01 16:34:06', '0', '', '1');
INSERT INTO `sys_menu` VALUES ('4', '1', '6', '角色管理', null, '0', 'sysRole!main.do', '&#xe61b;', '2017-05-27 11:24:12', null, 'sysRoleAction', '2');
INSERT INTO `sys_menu` VALUES ('6', '1', '7', '数据库监控', '', '0', 'druid/index.html', '&#xe636;', '2017-06-01 15:16:57', '0', '', '1');
INSERT INTO `sys_menu` VALUES ('7', '1', '8', '定时任务管理', '', '0', 'sysQuartz!main.do', '&#xe62c;', '2017-06-01 15:41:17', '0', '', '1');
INSERT INTO `sys_menu` VALUES ('5', '1', '9', '代码生成器', '', '0', 'easyCode!main.do', '&#xe60a;', '2017-06-01 15:44:32', '0', '', '1');
INSERT INTO `sys_menu` VALUES ('8', '1', '10', '日志管理', '', '0', 'sysLog!main.do', 'fa-book', '2017-06-06 10:15:46', '0', '', '1');

-- ----------------------------
-- Table structure for sys_menu_kind
-- ----------------------------
DROP TABLE IF EXISTS `sys_menu_kind`;
CREATE TABLE `sys_menu_kind` (
  `id` decimal(10,0) NOT NULL,
  `menu_id` decimal(10,0) DEFAULT NULL COMMENT '菜单Id',
  `kind_id` decimal(10,0) DEFAULT NULL COMMENT '种类Id',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_menu_kind
-- ----------------------------
INSERT INTO `sys_menu_kind` VALUES ('206', '87', '1', '2017-05-27 11:25:37');
INSERT INTO `sys_menu_kind` VALUES ('207', '87', '2', '2017-05-27 11:25:37');
INSERT INTO `sys_menu_kind` VALUES ('192', '4', '1', '2017-05-27 11:25:37');
INSERT INTO `sys_menu_kind` VALUES ('193', '4', '2', '2017-05-27 11:25:37');
INSERT INTO `sys_menu_kind` VALUES ('100064', '2', '4', '2017-06-01 15:41:26');
INSERT INTO `sys_menu_kind` VALUES ('100063', '2', '3', '2017-06-01 15:41:26');
INSERT INTO `sys_menu_kind` VALUES ('100062', '2', '2', '2017-06-01 15:41:26');
INSERT INTO `sys_menu_kind` VALUES ('100061', '2', '1', '2017-06-01 15:41:26');
INSERT INTO `sys_menu_kind` VALUES ('194', '4', '3', '2017-05-27 11:25:38');
INSERT INTO `sys_menu_kind` VALUES ('195', '4', '4', '2017-05-27 11:25:38');
INSERT INTO `sys_menu_kind` VALUES ('100067', '3', '2', '2017-06-01 16:34:06');
INSERT INTO `sys_menu_kind` VALUES ('100066', '3', '1', '2017-06-01 16:34:06');
INSERT INTO `sys_menu_kind` VALUES ('100057', '100053', '2', '2017-05-27 16:27:29');
INSERT INTO `sys_menu_kind` VALUES ('100056', '100053', '1', '2017-05-27 16:27:29');

-- ----------------------------
-- Table structure for sys_quartz
-- ----------------------------
DROP TABLE IF EXISTS `sys_quartz`;
CREATE TABLE `sys_quartz` (
  `id` decimal(10,0) NOT NULL,
  `triggername` varchar(40) DEFAULT NULL COMMENT '触发器名称',
  `cronexpression` varchar(40) DEFAULT NULL COMMENT '时间表达式',
  `jobdetailname` varchar(40) DEFAULT NULL COMMENT '任务名称',
  `targetobject` varchar(40) DEFAULT NULL COMMENT '目标名称',
  `methodname` varchar(40) DEFAULT NULL COMMENT '方法名称',
  `concurrent` decimal(10,0) DEFAULT '0' COMMENT '是否并发',
  `state` decimal(10,0) DEFAULT '0' COMMENT '状态',
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_quartz
-- ----------------------------
INSERT INTO `sys_quartz` VALUES ('1', '测试', '0/3 * * * * ?', 'detailname', 'testQuarz', 'haha', '1', '0', '2017-06-06 10:04:45');
INSERT INTO `sys_quartz` VALUES ('100069', '系统访问日志定时器', '0/30 * * * * ?', 'logInfo', 'logQuarz', 'execute', '1', '1', '2017-06-06 10:04:53');

-- ----------------------------
-- Table structure for sys_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role` (
  `id` decimal(10,0) NOT NULL,
  `name` varchar(200) DEFAULT NULL COMMENT '名称',
  `note` varchar(200) DEFAULT NULL COMMENT '说明',
  `creator` decimal(10,0) DEFAULT NULL COMMENT '创建人',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_role
-- ----------------------------
INSERT INTO `sys_role` VALUES ('1', '超级管理员', '超级管理员', '1', '2017-05-27 11:26:28');
INSERT INTO `sys_role` VALUES ('2', '测试角色', '测试角色', '1', '2017-05-27 11:26:28');
INSERT INTO `sys_role` VALUES ('0', '开发管理员', '开发管理员最高权限', '1', '2017-05-27 11:26:28');
INSERT INTO `sys_role` VALUES ('212', '测试2', '测试21', '2', '2017-05-27 11:26:28');

-- ----------------------------
-- Table structure for sys_role_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_user`;
CREATE TABLE `sys_role_user` (
  `id` decimal(10,0) NOT NULL,
  `roleid` decimal(10,0) DEFAULT NULL COMMENT '角色id',
  `userid` decimal(10,0) DEFAULT NULL COMMENT '用户id',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `creator` decimal(10,0) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_role_user
-- ----------------------------
INSERT INTO `sys_role_user` VALUES ('209', '0', '0', '2017-05-27 11:26:47', '2');
INSERT INTO `sys_role_user` VALUES ('210', '1', '2', '2017-05-27 11:26:47', '2');
INSERT INTO `sys_role_user` VALUES ('262', '2', '211', '2017-05-27 11:26:47', '211');
INSERT INTO `sys_role_user` VALUES ('263', '212', '211', '2017-05-27 11:26:47', '211');

-- ----------------------------
-- Table structure for sys_sequence
-- ----------------------------
DROP TABLE IF EXISTS `sys_sequence`;
CREATE TABLE `sys_sequence` (
  `name` varchar(50) NOT NULL COMMENT '序列的名字，唯一',
  `current_value` bigint(20) NOT NULL COMMENT '当前的值',
  `increment_value` int(11) NOT NULL DEFAULT '1' COMMENT '步长，默认为1',
  PRIMARY KEY (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_sequence
-- ----------------------------
INSERT INTO `sys_sequence` VALUES ('seq_id', '109591', '1');

-- ----------------------------
-- Table structure for sys_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user` (
  `id` decimal(10,0) NOT NULL DEFAULT '0',
  `username` varchar(50) NOT NULL,
  `name` varchar(50) DEFAULT NULL COMMENT '姓名',
  `password` varchar(100) DEFAULT NULL COMMENT '密码',
  `email` varchar(100) DEFAULT NULL COMMENT '邮箱',
  `phone` varchar(100) DEFAULT NULL COMMENT '电话',
  `state` varchar(1) DEFAULT '0' COMMENT '状态',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `creator` decimal(10,0) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_user
-- ----------------------------
INSERT INTO `sys_user` VALUES ('0', 'dev', 'dev', '202CB962AC59075B964B07152D234B70', '大神', null, '0', '2017-05-27 11:27:09', '0');
INSERT INTO `sys_user` VALUES ('211', 'test', '测试人员1', '098F6BCD4621D373CADE4E832627B4F6', null, '222', '0', '2017-05-27 11:27:09', '0');
INSERT INTO `sys_user` VALUES ('2', 'admin', 'admin', '21232F297A57A5A743894A0E4A801FC3', null, null, '0', '2017-05-27 16:32:53', '0');
INSERT INTO `sys_user` VALUES ('100044', 'test2', '测试2', 'E10ADC3949BA59ABBE56E057F20F883E', 'sdfsdf@sdf', '123123', '0', '2017-05-27 16:05:14', '0');

-- ----------------------------
-- Function structure for func_currval
-- ----------------------------
DROP FUNCTION IF EXISTS `func_currval`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` FUNCTION `func_currval`(seq_name varchar(50)) RETURNS int(11)
begin
 declare value integer;
 set value = 0;
 select current_value into value
 from sys_sequence
 where name = seq_name;
 return value;
end
;;
DELIMITER ;

-- ----------------------------
-- Function structure for func_nextval
-- ----------------------------
DROP FUNCTION IF EXISTS `func_nextval`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` FUNCTION `func_nextval`(seq_name varchar(50)) RETURNS int(11)
begin
 update sys_sequence
 set current_value = current_value + increment_value
 where name = seq_name;
 return func_currval(seq_name);
end
;;
DELIMITER ;
